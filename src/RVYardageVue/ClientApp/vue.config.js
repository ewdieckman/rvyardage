﻿module.exports = {
    // ...other vue-cli plugin options...
    pwa: {
        "name": "RV Yardage",
        workboxOptions: {
            skipWaiting: true   // in order to update on iOS
        }
    },
    configureWebpack: {
        resolve: {
            alias: {
                'numeric-keyboard$': 'numeric-keyboard/dist/numeric_keyboard.vue.js'
            }
        }
    }
}