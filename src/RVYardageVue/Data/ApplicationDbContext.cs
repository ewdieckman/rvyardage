﻿using Microsoft.EntityFrameworkCore;
using RVYardageVue.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RVYardageVue.Data
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {
            Database.EnsureCreated();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Play>()
                .HasMany(p => p.Tacklers);

            base.OnModelCreating(modelBuilder);
        }

        public DbSet<Team> Teams { get; set; }
        public DbSet<Game> Games { get; set; }
        public DbSet<Player> Players { get; set; }
        public DbSet<PassPlay> PassingPlays { get; set; }
        public DbSet<RunningPlay> RunningPlays { get; set; }
        public DbSet<KickoffPlay> KickOffPlays { get; set; }
        public DbSet<PuntPlay> PuntPlays { get; set; }

    }
}
