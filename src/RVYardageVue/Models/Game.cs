﻿using System;
using System.Collections.Generic;

namespace RVYardageVue.Models
{
    public class Game
    {
        public Game()
        {
            Plays = new List<Play>();
        }

        public int Id { get; set; }
        public Team HomeTeam { get; set; }
        public Team VisitingTeam { get; set; }
        public DateTime Date { get; set; }

        public List<Play> Plays { get; set; }
    }
}
