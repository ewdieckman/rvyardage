﻿using System.Collections.Generic;

namespace RVYardageVue.Models
{
    public class PuntPlay : Play
    {
        public Player Punter { get; set; }
        public Player Returner { get; set; }
    }
}
