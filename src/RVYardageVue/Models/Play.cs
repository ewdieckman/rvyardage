﻿using System.Collections.Generic;

namespace RVYardageVue.Models
{
    public abstract class Play
    {
        public Play()
        {
            Tacklers = new List<Player>();
        }
        public int Id { get; set; }
        // need a property to define "who's yardline" - as in, which side of the 50
        public int Yardline { get; set; }
        public int Down { get; set; }
        public int YardsToGo { get; set; }
        public List<Player> Tacklers { get; set; }
    }
}
