﻿using System;
using System.Collections.Generic;

namespace RVYardageVue.Models
{
    public class Team
    {
        public Team()
        {
            Roster = new List<Player>();
        }
        public int Id { get; set; }
        public string Mascot { get; set; }
        public string School { get; set; }
        public DateTime Date { get; set; }

        public List<Player> Roster { get; set; }
    }
}
