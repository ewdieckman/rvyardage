﻿using System.Collections.Generic;

namespace RVYardageVue.Models
{
    public class PassPlay : Play
    {
        public Player Thrower { get; set; }
        public Player Receiver { get; set; }
    }
}
