﻿using System.Collections.Generic;

namespace RVYardageVue.Models
{
    public class KickoffPlay : Play
    {
        public Player Kickoff { get; set; }
        public Player Returner { get; set; }
    }
}
