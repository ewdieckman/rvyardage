﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace RVYardageVue.Migrations
{
    public partial class puntkickoff : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "KickoffId",
                table: "Play",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "PuntPlay_ReturnerId",
                table: "Play",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "PunterId",
                table: "Play",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "ReturnerId",
                table: "Play",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Play_KickoffId",
                table: "Play",
                column: "KickoffId");

            migrationBuilder.CreateIndex(
                name: "IX_Play_PunterId",
                table: "Play",
                column: "PunterId");

            migrationBuilder.CreateIndex(
                name: "IX_Play_PuntPlay_ReturnerId",
                table: "Play",
                column: "PuntPlay_ReturnerId");

            migrationBuilder.CreateIndex(
                name: "IX_Play_ReturnerId",
                table: "Play",
                column: "ReturnerId");

            migrationBuilder.AddForeignKey(
                name: "FK_Play_Players_KickoffId",
                table: "Play",
                column: "KickoffId",
                principalTable: "Players",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Play_Players_PunterId",
                table: "Play",
                column: "PunterId",
                principalTable: "Players",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Play_Players_PuntPlay_ReturnerId",
                table: "Play",
                column: "PuntPlay_ReturnerId",
                principalTable: "Players",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Play_Players_ReturnerId",
                table: "Play",
                column: "ReturnerId",
                principalTable: "Players",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Play_Players_KickoffId",
                table: "Play");

            migrationBuilder.DropForeignKey(
                name: "FK_Play_Players_PunterId",
                table: "Play");

            migrationBuilder.DropForeignKey(
                name: "FK_Play_Players_PuntPlay_ReturnerId",
                table: "Play");

            migrationBuilder.DropForeignKey(
                name: "FK_Play_Players_ReturnerId",
                table: "Play");

            migrationBuilder.DropIndex(
                name: "IX_Play_KickoffId",
                table: "Play");

            migrationBuilder.DropIndex(
                name: "IX_Play_PunterId",
                table: "Play");

            migrationBuilder.DropIndex(
                name: "IX_Play_PuntPlay_ReturnerId",
                table: "Play");

            migrationBuilder.DropIndex(
                name: "IX_Play_ReturnerId",
                table: "Play");

            migrationBuilder.DropColumn(
                name: "KickoffId",
                table: "Play");

            migrationBuilder.DropColumn(
                name: "PuntPlay_ReturnerId",
                table: "Play");

            migrationBuilder.DropColumn(
                name: "PunterId",
                table: "Play");

            migrationBuilder.DropColumn(
                name: "ReturnerId",
                table: "Play");
        }
    }
}
