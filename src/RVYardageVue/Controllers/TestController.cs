﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using RVYardageVue.Data;
using RVYardageVue.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RVYardageVue.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TestController : ControllerBase
    {
        private readonly ApplicationDbContext _dbContext;

        public TestController(ApplicationDbContext dbContext)
        {
            if (dbContext == null)
                throw new ArgumentNullException("dbContext");
            _dbContext = dbContext;
        }

        [HttpGet("~/test")]
        public async Task<IActionResult> Test()
        {
            var rv = await _dbContext.Teams.Where(t => t.Id == 1).FirstOrDefaultAsync();
            var ew = await _dbContext.Teams.Where(t => t.Id == 3).FirstOrDefaultAsync();
            var game = await _dbContext.Games.Where(g => g.HomeTeam == rv && g.VisitingTeam == ew).FirstOrDefaultAsync();

            var wickman = await _dbContext.Players.Where(p => p.Number == 2 && p.Team == rv).FirstOrDefaultAsync();
            var klest = await _dbContext.Players.Where(p => p.Number == 44 && p.Team == ew).FirstOrDefaultAsync();
            var vickerman = await _dbContext.Players.Where(p => p.Number == 18 && p.Team == rv).FirstOrDefaultAsync();

            var play = new RunningPlay()
            {
                Runner = wickman,
                Yardline = 40,
                YardsToGo = 4,
                Down = 2
            };
            play.Tacklers.Add(klest);

            game.Plays.Add(play);

            await _dbContext.SaveChangesAsync();

            var passingPlay = new PassPlay()
            {
                Thrower = wickman,
                Receiver = vickerman,
                Yardline = 25,
                YardsToGo = 10,
                Down = 1

            };
            passingPlay.Tacklers.Add(klest);

            game.Plays.Add(passingPlay);

            await _dbContext.SaveChangesAsync();

            return Ok();
        }
    }
}
